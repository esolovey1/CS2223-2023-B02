package edu.wpi.cs2223.algorithms.week6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DigraphBreadthFirstTest {
    @Test
    public void csClasses(){
        DiGraph diGraph = new BasicDiGraph(7);
        diGraph.addEdge(0, 1);
        diGraph.addEdge(0, 2);
        diGraph.addEdge(0, 5);
        diGraph.addEdge(1, 4);
        diGraph.addEdge(1, 4);
        diGraph.addEdge(3, 2);
        diGraph.addEdge(3, 4);
        diGraph.addEdge(3, 5);
        diGraph.addEdge(3, 6);
        diGraph.addEdge(5, 2);
        diGraph.addEdge(6, 4);

        DigraphBreadthFirst digraphBreadthFirst = new DigraphBreadthFirst(diGraph);

        for (Integer vertex : digraphBreadthFirst.reversePreOrder){
            System.out.println(vertex);
        }
    }

}