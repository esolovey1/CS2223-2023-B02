package edu.wpi.cs2223.algorithms.week6;

import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

class DepthFirstPathsTest {
    @Test
    public void graphOne(){
        Graph graph = new BasicGraph(13);

        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(0, 6);
        graph.addEdge(6, 4);
        graph.addEdge(4, 5);
        graph.addEdge(5, 3);

        graph.addEdge(7, 8);
        graph.addEdge(9, 10);
        graph.addEdge(9, 12);
        graph.addEdge(9, 11);

        DepthFirstPaths paths = new DepthFirstPaths(graph, 0);
        Iterator<Integer> pathToFive = paths.pathTo(5);

        assertEquals(0, pathToFive.next());
        assertEquals(6, pathToFive.next());
        assertEquals(4, pathToFive.next());
        assertEquals(5, pathToFive.next());
        assertFalse(pathToFive.hasNext());
    }

}