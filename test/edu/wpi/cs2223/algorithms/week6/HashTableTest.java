package edu.wpi.cs2223.algorithms.week6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class HashTableTest {
    HashTable hashTable;

    @BeforeEach
    public void setUp(){
         hashTable = new HashTable();
    }

    @Test
    void hashKeyDistribution(){
        Map<Integer, Integer> hashCounts = new HashMap<>();

        Set<String> inputKeys = new HashSet<>();
        for (int a = 0; a < 10_000; a++) {
            byte[] array = new byte[7];
            new Random().nextBytes(array);
            inputKeys.add(new String(array, StandardCharsets.UTF_8));
        }

        for (String key : inputKeys) {
            int hashKey = hashTable.hashKey(key);
            hashCounts.put(hashKey, hashCounts.containsKey(hashKey) ? hashCounts.get(hashKey) + 1 : 1);
        }

        System.out.println(hashCounts);
    }

}