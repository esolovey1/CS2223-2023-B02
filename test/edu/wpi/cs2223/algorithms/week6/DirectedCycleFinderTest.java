package edu.wpi.cs2223.algorithms.week6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DirectedCycleFinderTest {
    @Test
    public void emptyGraph(){
        DiGraph diGraph = new BasicDiGraph(0);
        DirectedCycleFinder cycleFinder = new DirectedCycleFinder(diGraph);

        assertFalse(cycleFinder.hasCycle());
    }

    @Test
    public void singleNode(){
        DiGraph diGraph = new BasicDiGraph(1);
        DirectedCycleFinder cycleFinder = new DirectedCycleFinder(diGraph);

        assertFalse(cycleFinder.hasCycle());
    }

    @Test
    public void twoNodes_noCycle(){
        DiGraph diGraph = new BasicDiGraph(2);
        diGraph.addEdge(0, 1);

        DirectedCycleFinder cycleFinder = new DirectedCycleFinder(diGraph);

        assertFalse(cycleFinder.hasCycle());
    }

    @Test
    public void twoNodes_hasCycle(){
        DiGraph diGraph = new BasicDiGraph(2);
        diGraph.addEdge(0, 1);
        diGraph.addEdge(1, 0);

        DirectedCycleFinder cycleFinder = new DirectedCycleFinder(diGraph);

        assertTrue(cycleFinder.hasCycle());
        assertEquals("0 - 1 - 0", cycleFinder.firstCycle());
    }

    @Test
    public void multipleNodes_noCycles(){
        DiGraph diGraph = new BasicDiGraph(6);
        diGraph.addEdge(0, 1);
        diGraph.addEdge(1, 2);
        diGraph.addEdge(2, 3);
        diGraph.addEdge(2, 4);
        diGraph.addEdge(2, 5);
        diGraph.addEdge(3, 4);

        DirectedCycleFinder cycleFinder = new DirectedCycleFinder(diGraph);

        assertFalse(cycleFinder.hasCycle());
    }

    @Test
    public void multipleNodes_hasCycle(){
        DiGraph diGraph = new BasicDiGraph(7);
        diGraph.addEdge(0, 1);
        diGraph.addEdge(1, 2);
        diGraph.addEdge(2, 3);
        diGraph.addEdge(2, 4);
        diGraph.addEdge(2, 6);
        diGraph.addEdge(3, 5);
        diGraph.addEdge(5, 1);

        DirectedCycleFinder cycleFinder = new DirectedCycleFinder(diGraph);

        assertTrue(cycleFinder.hasCycle());
        assertEquals("1 - 2 - 3 - 5 - 1", cycleFinder.firstCycle());
    }

}