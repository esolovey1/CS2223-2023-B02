package edu.wpi.cs2223.algorithms.shared;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.*;

public class QueueIterationTest {
    LinkedListQueue<String> queue;

    @BeforeEach
    public void setUp(){
        queue = new LinkedListQueue<>();
    }

    @Test
    public void iterator_simple(){
        queue.enqueue("first");
        queue.enqueue("second");
        queue.enqueue("third");

        String concatenation = "";
        for (String value : queue){
            concatenation += value + " ";
        }

        assertEquals("first second third ", concatenation);
    }

    @Test
    public void iterator_afterOperations(){
        queue.enqueue("first");
        queue.enqueue("second");
        queue.enqueue("third");

        queue.dequeue();

        queue.enqueue("fourth");
        queue.enqueue("fifth");

        String concatenation = "";
        for (String value : queue){
            concatenation += value + " ";
        }

        assertEquals("second third fourth fifth ", concatenation);
    }

    @Test()
    public void iterator_withStateChange(){
        queue.enqueue("first");
        queue.enqueue("second");
        queue.enqueue("third");
        queue.enqueue("fourth");
        queue.enqueue("fifth");


        String concatenation = "";
        Iterator<String> iterator = queue.iterator();

        concatenation += iterator.next() + " ";
        concatenation += iterator.next() + " ";
        concatenation += iterator.next() + " ";

        assertEquals("first second third ", concatenation);
        assertTrue(iterator.hasNext());

        queue.dequeue();
        queue.dequeue();

        // if the iterator did not throw here, hasNext would return true!!
        assertThrows(ConcurrentModificationException.class, iterator::hasNext);
    }
}
