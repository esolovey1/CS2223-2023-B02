package edu.wpi.cs2223.algorithms.week2;

import edu.wpi.cs2223.algorithms.week3.MergeSort;
import edu.wpi.cs2223.algorithms.week3.QuickSort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class SortAlgorithmTest {
    SortAlgorithm sortAlgorithm;

    @BeforeEach
    public void setUp(){
        sortAlgorithm = new QuickSort();
    }

    @Test
    public void emptyArray(){
        Integer[] input = new Integer[0];
        sortAlgorithm.sort(input);

        assertEquals(0, input.length);
    }

    @Test
    public void singleElementArray(){
        Integer[] input = new Integer[] { 5 };
        sortAlgorithm.sort(input);

        assertEquals(1, input.length);
        assertEquals(5, input[0]);
    }

    @Test
    public void twoElementsAlreadySortedArray(){
        Integer[] input = new Integer[] { 5, 9 };
        Integer[] expectedOutput = new Integer[] { 5, 9 };

        sortAlgorithm.sort(input);

        assertArrayEquals(expectedOutput, input);
    }

    @Test
    public void twoElementsNotYetSortedArray(){
        Integer[] input = new Integer[] { 9, 5 };
        Integer[] expectedOutput = new Integer[] { 5, 9 };

        sortAlgorithm.sort(input);

        assertArrayEquals(expectedOutput, input);
    }

    @Test
    public void threeElementsNotYetSortedArray(){
        Integer[] input = new Integer[] { 8, 10, 9 };
        Integer[] expectedOutput = new Integer[] { 8, 9, 10 };

        sortAlgorithm.sort(input);

        assertArrayEquals(expectedOutput, input);
    }

    @Test
    public void mediumUnsortedArray(){
        Integer[] input = new Integer[] { 9, 5, 1, 10, 6, 8, 3 };
        Integer[] expectedOutput = new Integer[] { 1, 3, 5, 6, 8, 9, 10 };

        sortAlgorithm.sort(input);

        assertArrayEquals(expectedOutput, input);
    }

    @Test
    public void classWalkThroughArray(){
        Integer[] input = new Integer[] { 3, 1, 9, 7, 2, 6, 10, 8, 5, 4 };
        Integer[] expectedOutput = new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        sortAlgorithm.sort(input);

        assertArrayEquals(expectedOutput, input);
    }

    @Test
    public void largeUnsortedArray(){
        Integer[] input = new Integer[] { 9, 5, 1, 63, 10, 6, 29, 21, 88, 2, 8, 3, 33, 100, 78, 91, 4, 18, 7, 77 };
        Integer[] expectedOutput = new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 18, 21, 29, 33, 63, 77, 78, 88, 91, 100 };

        sortAlgorithm.sort(input);

        assertArrayEquals(expectedOutput, input);
    }

    // Uncomment to execute performance testing
//    @Test
    public void doublingPerformanceTest(){
        for (int n = 1; n < Math.pow(2, 14); n = n * 2) {
            sortAlgorithm.sort(constructRandomList(n));

            System.out.printf(
                    "Input with %d elements sorted with %d comparisons and %d exchanges.\n",
                    n, sortAlgorithm.numberOfComparisons(),
                    sortAlgorithm.numberOfExchanges());
        }
    }

    // This test is commented out because stability is not a requirement of all sorting algorithms - rather it is
    // an interesting property of them.  For example, uncommenting this test for SelectionSort should see it fail
    // because the firstFour will get swapper into a position after the firstFour on the very first iteration
    // (when one is moved to the zero index spot)
//    @Test
    public void stabilityTest(){
        Integer one = new Integer(1);
        Integer two = new Integer(2);

        Integer firstFour = new Integer(4);
        Integer secondFour = new Integer(4);

        Integer[] input = new Integer[] { firstFour, two, secondFour, one };

        sortAlgorithm.sort(input);

        assertTrue(one == input[0]);
        assertTrue(two == input[1]);
        assertTrue(firstFour == input[2]);
        assertTrue(secondFour == input[3]);
    }

    Integer[] constructRandomList(int size) {
        Integer[] array = new Integer[size];
        Random random = new Random();


        for (int i = 0; i < size; i ++) {
            array[i] = random.nextInt();
        }

        return array;
    }
}
