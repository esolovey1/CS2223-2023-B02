package edu.wpi.cs2223.algorithms.week1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DijkstrasCalculatorTest {

    DijkstrasCalculator calculator;

    @BeforeEach
    public void setUp(){
        calculator = new DijkstrasCalculator();
    }

    @Test
    public void simpleEval(){
        assertEquals(11, calculator.evaluate("( 3 + 8 )"));
    }

    @Test
    public void multipleExpressions(){
        assertEquals(58, calculator.evaluate("((( 3 + 8 ) * 5 ) + ((4 * 12) - 45 ))"));
    }

    @Test
    public void multipleExpressionsWithSquareRoot(){
        assertEquals(9, calculator.evaluate("(r ((3 * 10) + ((10 + 7) * 3)))"));
    }
}