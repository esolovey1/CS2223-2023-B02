package edu.wpi.cs2223.algorithms.week3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayMinPriorityQueueTest {
    ArrayMinPriorityQueue<Integer> priorityQueue;

    @BeforeEach
    public void setUp(){
        priorityQueue = new ArrayMinPriorityQueue<>(8);
    }

    @Test
    public void emptyQueue(){
        assertEquals(0, priorityQueue.size);
        assertTrue(priorityQueue.isEmpty());
        assertNull(priorityQueue.removeMin());
    }

    @Test
    public void singleElementQueue(){
        priorityQueue.insert(23);

        assertEquals(1, priorityQueue.size);
        assertFalse(priorityQueue.isEmpty());

        assertEquals(23, priorityQueue.removeMin());
    }

    @Test
    public void kruskal() {
        priorityQueue = new ArrayMinPriorityQueue<>(16);
        priorityQueue.insert(6);
        priorityQueue.insert(12);
        priorityQueue.insert(10);
        priorityQueue.insert(8);
        priorityQueue.insert(14);
        priorityQueue.insert(16);
        priorityQueue.insert(9);
        priorityQueue.insert(11);
        priorityQueue.insert(20);
        priorityQueue.insert(7);
        priorityQueue.insert(13);
        priorityQueue.insert(17);
        priorityQueue.insert(15);
        priorityQueue.insert(21);
        priorityQueue.insert(5);
        priorityQueue.insert(18);

        System.out.println(priorityQueue.removeMin());
        System.out.println(priorityQueue.removeMin());
        System.out.println(priorityQueue.removeMin());
        System.out.println(priorityQueue.removeMin());
        System.out.println(priorityQueue.removeMin());
        System.out.println(priorityQueue.removeMin());
        System.out.println(priorityQueue.removeMin());
        System.out.println(priorityQueue.removeMin());
        System.out.println(priorityQueue.removeMin());
        System.out.println(priorityQueue.removeMin());
        System.out.println(priorityQueue.removeMin());
        System.out.println(priorityQueue.removeMin());
        System.out.println(priorityQueue.removeMin());
    }

    @Test
    public void swim(){
        priorityQueue.nodes = new Integer[]{0, 3, 18, 21, 24, 23, 33, 2};
        priorityQueue.swim(7);

        assertEquals(2, priorityQueue.nodes[1]);
        assertEquals(3, priorityQueue.nodes[3]);
    }

    @Test
    public void insertionsAndRemovalsBeyondMaxSize(){
        priorityQueue.insert(9);
        priorityQueue.insert(4);
        priorityQueue.insert(23);
        priorityQueue.insert(18);
        priorityQueue.insert(10);
        priorityQueue.insert(7);
        priorityQueue.insert(1);
        priorityQueue.insert(20);

        assertEquals(8, priorityQueue.size);

        assertEquals(1, priorityQueue.removeMin());
        priorityQueue.insert(1);
        assertEquals(8, priorityQueue.size);

        // insert a value that should not stay because it is smaller than min value
        priorityQueue.insert(-1);

        assertEquals(8, priorityQueue.size);
        assertEquals(1, priorityQueue.removeMin());
        priorityQueue.insert(1);
        assertEquals(8, priorityQueue.size);

        // insert a value that should stay and should evict our current min
        priorityQueue.insert(2);
        assertEquals(8, priorityQueue.size);
        assertEquals(2, priorityQueue.removeMin());
        priorityQueue.insert(2);
        assertEquals(8, priorityQueue.size);

        // insert a value that should stay and should evict our current min, but is not the new min
        priorityQueue.insert(19);
        assertEquals(8, priorityQueue.size);
        assertEquals(4, priorityQueue.removeMin());
        priorityQueue.insert(4);
        assertEquals(8, priorityQueue.size);

    }

    @Test
    public void insertionsAndRemovals(){
        priorityQueue.insert(5);
        priorityQueue.insert(2);

        assertFalse(priorityQueue.isEmpty());
        assertEquals(2, priorityQueue.size);

        assertEquals(2, priorityQueue.removeMin());
        assertEquals(1, priorityQueue.size);

        priorityQueue.insert(7);
        priorityQueue.insert(1);
        priorityQueue.insert(20);

        assertEquals(4, priorityQueue.size);

        assertEquals(1, priorityQueue.removeMin());
        assertEquals(3, priorityQueue.size);


        priorityQueue.insert(9);
        priorityQueue.insert(4);
        priorityQueue.insert(23);
        priorityQueue.insert(18);
        priorityQueue.insert(10);

        assertEquals(8, priorityQueue.size);

        assertEquals(4, priorityQueue.removeMin());
        assertEquals(5, priorityQueue.removeMin());
        assertEquals(7, priorityQueue.removeMin());
        assertEquals(9, priorityQueue.removeMin());
        assertEquals(10, priorityQueue.removeMin());
        assertEquals(18, priorityQueue.removeMin());
        assertEquals(20, priorityQueue.removeMin());
        assertEquals(23, priorityQueue.removeMin());

        assertEquals(0, priorityQueue.size);
        assertTrue(priorityQueue.isEmpty());
    }


}