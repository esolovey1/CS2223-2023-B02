package edu.wpi.cs2223.algorithms.week3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QuickSortTest {
    QuickSort<Integer> quickSort;

    @BeforeEach
    public void setUp(){
        quickSort = new QuickSort<>();
    }

    @Test
    public void partitionSingleElement(){
        Integer[] input = new Integer[] { 10 };

        int result = quickSort.partition(input, 0, 0);

        assertEquals(0, result);
        assertArrayEquals(new Integer[] { 10 }, input);
    }

    @Test
    public void partitionLongArray(){
        Integer[] input = new Integer[] { 19, 3, 10, 22, 7, 99, 71, 55, 41, 29 };

        int result = quickSort.partition(input, 0, 9);

        assertEquals(3, result);
        assertArrayEquals(new Integer[] { 7, 3, 10, 19, 22, 99, 71, 55, 41, 29 }, input);
    }

    @Test
    public void partitionLongArrayMiddle(){
        Integer[] input = new Integer[] { 19, 3, 10, 48, 7, 99, 71, 55, 41, 29 };

        int result = quickSort.partition(input, 3, 9);

        assertEquals(6, result);
        assertArrayEquals(new Integer[] { 19, 3, 10, 41, 7, 29, 48, 55, 71, 99 }, input);
    }

    @Test
    public void selectLongArraySecond(){
        Integer[] input = new Integer[] { 19, 3, 10, 48, 7, 99, 71, 55, 41, 29 };
        int result = quickSort.select(input, 2);

        assertEquals(7, result);
    }

    @Test
    public void selectLongArrayMedian(){
        Integer[] input = new Integer[] { 19, 3, 10, 48, 7, 99, 71, 55, 41, 29 };
        int result = quickSort.select(input, 5);

        assertEquals(29, result);
    }
}