package edu.wpi.cs2223.algorithms.week3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MergeSortTest {
    MergeSort mergeSort;

    @BeforeEach
    public void setUp(){
        mergeSort = new MergeSort();
    }

    @Test
    public void mergeBothEmpty(){
        Integer[] input = new Integer[0];
        Integer[] aux = new Integer[0];

        mergeSort.merge(input, aux, 0, 0, 0);
        assertEquals(0, input.length);
    }

    @Test
    public void mergeOneEmptyOneSingle(){
        Integer[] input = new Integer[] { 5 };
        Integer[] aux = new Integer[1];

        mergeSort.merge(input, aux, 0, 0, 0);
        assertArrayEquals(new Integer[] { 5 }, input);
    }

    @Test
    public void mergeTwoSingle(){
        Integer[] input = new Integer[] { 5, 1 };
        Integer[] aux = new Integer[2];

        mergeSort.merge(input, aux, 0, 0, 1);
        assertArrayEquals(new Integer[] { 1, 5 }, input);
    }

    @Test
    public void mergeOneSingleOneLarge(){
        Integer[] input = new Integer[] { 1, 3, 8, 11, 6 };
        Integer[] aux = new Integer[5];

        mergeSort.merge(input, aux, 0, 3, 4);
        assertArrayEquals(new Integer[] { 1, 3, 6, 8, 11 }, input);
    }

    @Test
    public void mergeTwoLarge(){
        Integer[] input = new Integer[] { 1, 3, 8, 11, 6, 7, 9, 10, 13, 22 };
        Integer[] aux = new Integer[10];

        mergeSort.merge(input, aux, 0, 3, 9);
        assertArrayEquals(new Integer[] { 1, 3, 6, 7, 8, 9, 10, 11, 13, 22 }, input);
    }

}