package edu.wpi.cs2223.algorithms.week3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeapSortTest {
        HeapSort<Integer> heapSort;

    @BeforeEach
    public void setUp(){
        heapSort = new HeapSort<>();
    }

    @Test
    public void sortEmpty(){

        // in this case the agreement that indecies 1 through n are sorted
        Integer[] input = new Integer[]{ 22 };
        heapSort.sort(input);

        assertArrayEquals(new Integer[]{ 22  }, input);
    }

    @Test
    public void sortSingle(){
        // in this case the agreement that indecies 1 through n are sorted
        Integer[] input = new Integer[]{ 22, 90 };
        heapSort.sort(input);

        assertArrayEquals(new Integer[]{ 22, 90  }, input);
    }

    @Test
    public void sortTrivial(){
        // in this case the agreement that indecies 1 through n are sorted
        Integer[] input = new Integer[]{ 22, 90, 110 };
        heapSort.sort(input);

        assertArrayEquals(new Integer[]{ 22, 110, 90 }, input);
    }

    @Test
    public void sortMediumPowerOfTwo(){
        Integer[] input = new Integer[]{ 22, 3, 19, 11, 99, 24, 1, 9 };
        heapSort.sort(input);

        assertArrayEquals(new Integer[]{ 22, 99, 24, 19, 11, 9, 3, 1 }, input);
    }

    @Test
    public void sortLargerNotPowerOfTwo(){
        Integer[] input = new Integer[]{ -1, 88, 32, 95, 13, 100, 103, 4, 2, 56, 81 };
        heapSort.sort(input);

        assertArrayEquals(new Integer[]{ -1, 103, 100, 95, 88, 81, 56, 32, 13, 4, 2 }, input);
    }
}