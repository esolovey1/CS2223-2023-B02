package edu.wpi.cs2223.algorithms.week3;

import edu.wpi.cs2223.algorithms.week2.SortAlgorithm;

/**
 * Implementation of the heap sort algorithm.  This implementation takes advantage of the fact that it
 * is in the same package as our {@link ArrayMinPriorityQueue} implementation and manipulates that state
 * directly.
 */
public class HeapSort<T extends Comparable<T>> implements SortAlgorithm<T> {

    @Override
    public void sort(T[] input) {
        ArrayMinPriorityQueue<T> priorityQueue = new ArrayMinPriorityQueue<>(input);

        // create heap
        for (int a = input.length / 2; a >= 1; a--){
            priorityQueue.sink(a);
        }

        // move elements into position
        for (int indexToSwap = input.length - 1; indexToSwap > 1; indexToSwap--){
            // move smallest element into last spot
            priorityQueue.exchange(1, indexToSwap);

            // exclude that smallest element from further consideration
            priorityQueue.size--;

            // then re-heapify result
            priorityQueue.sink(1);
        }
    }

    @Override
    public void exchange(T[] array, int indexOne, int indexTwo) {

    }

    @Override
    public int numberOfExchanges() {
        // TODO: implement
        return 0;
    }

    @Override
    public int numberOfComparisons() {
        // TODO: implement
        return 0;
    }
}
