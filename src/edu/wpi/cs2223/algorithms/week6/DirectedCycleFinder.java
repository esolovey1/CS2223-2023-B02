package edu.wpi.cs2223.algorithms.week6;

import edu.wpi.cs2223.algorithms.shared.FixedCapacityArrayStack;

/**
 * Determines whether a given directed graph has any cycles (path of 2 or more nodes that starts and ends
 * at the same node, and does not visit any edges more than once).  If the graph has at least one cycle, this class will
 * return the nodes that make up the first cycle that it finds.
 */
public class DirectedCycleFinder {
    final DiGraph diGraph;

    final boolean[] marked;
    final boolean[] completed;

    boolean hasCycle = false;

    /**
     * When the graph has a cycle, this string should be populated in the form:
     *  3 - 4 - 1 - 3
     *  to denote a cycle that starts at node 3 goes to node 4 then node 1 and back to node 3 to complete the cycle
     */
    String cycle;

    public DirectedCycleFinder(DiGraph diGraph) {
        this.diGraph = diGraph;

        this.marked = new boolean[diGraph.numberOfVertices()];
        this.completed = new boolean[diGraph.numberOfVertices()];

        for (int a = 0; a < diGraph.numberOfVertices(); a++){
            marked[a] = false;
            completed[a] = false;
        }

        // because the graph may not be fully connected, look for cycles starting from every vertex that has not
        // yet been marked
        for (int vertex = 0; vertex < diGraph.numberOfVertices(); vertex++){
            if (!marked[vertex]) {
                lookForCycle(vertex, new FixedCapacityArrayStack<Integer>(diGraph.numberOfVertices() + 1));
            }
        }
    }

    void lookForCycle(int vertex, FixedCapacityArrayStack<Integer> path) {
        // TODO: implement
    }

    /**
     * @return String representation of the vertex path in the path stack.
     */
    String pathToString(FixedCapacityArrayStack<Integer> path, int cycleNode) {
        int cycleNodeSeenTimes = 0;

        String cycle = "";
        while (!path.isEmpty()) {
            int nodeOnPath = path.pop();

            if (nodeOnPath == cycleNode) {
                cycleNodeSeenTimes++;
            }

            cycle = (path.isEmpty() || cycleNodeSeenTimes > 1 ? "" : " - ") + nodeOnPath + cycle;

            if (cycleNodeSeenTimes > 1) {
                return cycle;
            }
        }

        return cycle;
    }

    public boolean hasCycle(){
        return hasCycle;
    }

    public String firstCycle(){
        return cycle;
    }
}
