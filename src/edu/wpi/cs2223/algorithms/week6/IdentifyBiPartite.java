package edu.wpi.cs2223.algorithms.week6;

/**
 * Determines if a given graph is bipartite or not.  A graph is bipartite, if its vertices can be divided into two
 * sets such that all edges go from nodes in set one to nodes in set two and there are no intraset edges.
 */
public class IdentifyBiPartite {
    enum Color {
        RED, GREEN;

        public Color otherColor(){
            return (this == RED) ? GREEN : RED;
        }
    }

    final Graph graph;
    final Color[] colors;

    final boolean isBiPartite;

    public IdentifyBiPartite(Graph graph) {
        this.graph = graph;
        this.colors = new Color[graph.numberOfVertices()];

        for (int a = 0; a < graph.numberOfVertices(); a++) {
            this.colors[a] = null;
        }

        if (graph.numberOfVertices() == 0) {
            isBiPartite = true;
            return;
        }

        isBiPartite = dfsBipartite(0, Color.RED);
    }

    boolean dfsBipartite(int vertex, Color color){
        // TODO: implement
        return false;
    }

    public boolean isBiPartite() {
        return isBiPartite;
    }

    public Color vertexColor(int vertex) {
        return colors[vertex];
    }
}
