package edu.wpi.cs2223.algorithms.week6;

import edu.wpi.cs2223.algorithms.week4.SymbolTable;

/**
 * Array backed hash table that resolves collisions via the linear probing technique.s
 */
public class LinearProbingHashTable<Key extends Comparable, Value> implements SymbolTable<Key, Value> {
    final int numberOfSlots;
    final Key[] keys;
    final Value[] values;

    int size = 0;

    public LinearProbingHashTable(int numberOfSlots) {
        this.numberOfSlots = numberOfSlots;

        keys = (Key[]) new Comparable[numberOfSlots];
        values = (Value[]) new Comparable[numberOfSlots];
    }

    @Override
    public void put(Key key, Value value) {
        int index = indexForKey(key);

        if (key.equals(keys[index])) {
            values[index] = value;
            return;
        }

        keys[index] = key;
        values[index] = value;
        size++;
    }

    @Override
    public Value get(Key key) {
        int index = indexForKey(key);
        return values[index];
    }

    @Override
    public Value delete(Key key) {
        // TODO: implement
        return null;
    }

    @Override
    public boolean contains(Key key) {
        int index = indexForKey(key);
        return keys[index] != null;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public int size() {
        return size;
    }

    /**
     * @return the index where the given key is stored after hashing and linear probing has been applied;
     * null if key is not present in this symbol table
     */
    int indexForKey(Key key) {
        int index = (key.hashCode() & 0x7fffffff) % numberOfSlots;
        while (keys[index] != null && !key.equals(keys[index])) {
            index = (index + 1) % numberOfSlots;
        }

        return index;
    }
}
