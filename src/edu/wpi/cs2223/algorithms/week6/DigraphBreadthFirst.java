package edu.wpi.cs2223.algorithms.week6;

import edu.wpi.cs2223.algorithms.shared.FixedCapacityArrayStack;
import edu.wpi.cs2223.algorithms.shared.LinkedListQueue;

public class DigraphBreadthFirst {
    final DiGraph diGraph;
    final boolean[] marked;

    final FixedCapacityArrayStack<Integer> reversePostOrder;
    final LinkedListQueue<Integer> postOrder;

    final FixedCapacityArrayStack<Integer> reversePreOrder;
    final LinkedListQueue<Integer> preOrder;

    public DigraphBreadthFirst(DiGraph diGraph) {
        this.diGraph = diGraph;
        this.marked = new boolean[diGraph.numberOfVertices()];

        for (int a = 0; a < diGraph.numberOfVertices(); a++){
            marked[a] = false;
        }

        this.reversePostOrder = new FixedCapacityArrayStack<>(diGraph.numberOfVertices());
        this.postOrder = new LinkedListQueue<>();

        this.reversePreOrder = new FixedCapacityArrayStack<>(diGraph.numberOfVertices());
        this.preOrder = new LinkedListQueue<>();

        for (int a = 0; a < diGraph.numberOfVertices(); a++){
            if (!marked[a]) {
                depthFirstSearch(a);
            }
        }
    }

    void depthFirstSearch(int vertex) {
        marked[vertex] = true;

        preOrder.enqueue(vertex);
        reversePreOrder.push(vertex);

        for (int linkedVertex : diGraph.adjacentVertices(vertex)) {
            if (!marked[linkedVertex]) {
                depthFirstSearch(linkedVertex);
            }
        }

        reversePostOrder.push(vertex);
        postOrder.enqueue(vertex);
    }
}
