package edu.wpi.cs2223.algorithms.week6;

import edu.wpi.cs2223.algorithms.shared.FixedCapacityArrayStack;
import edu.wpi.cs2223.algorithms.shared.LinkedListQueue;

import java.util.Iterator;

/**
 * For a given graph and vertex of interest, identifies paths to all other vertices in the graph from that vertex via
 * a breadth first search.
 */
public class BreadthFirstPaths {
    final Graph graph;
    final int vertexOfInterest;

    final boolean[] marked;
    final int[] edgeTo;

    public BreadthFirstPaths(Graph graph, int vertexOfInterest) {
        this.graph = graph;
        this.vertexOfInterest = vertexOfInterest;

        this.marked = new boolean[graph.numberOfVertices()];
        this.edgeTo = new int[graph.numberOfVertices()];

        for (int a = 0; a < graph.numberOfVertices(); a++){
            edgeTo[a] = -1;
            marked[a] = false;
        }

        breadthFirstSearch(vertexOfInterest);
    }

    void breadthFirstSearch(int vertex) {
        // TODO: implement
    }

    /**
     * @return if there is a path from vertexOfInterest to vertex in the graph.
     */
    public boolean hasPathTo(int vertex) {
        return marked[vertex];
    }

    /**
     * @return an iterable over the vertices on the path from vertexOfInterest to vertex in the graph.
     */
    public Iterable<Integer> pathTo(int vertex) {
        if (!marked[vertex]) {
            return new FixedCapacityArrayStack<Integer>(0);
        }

        FixedCapacityArrayStack<Integer> pathStack = new FixedCapacityArrayStack<Integer>(graph.numberOfVertices());
        int currentIndex = vertex;

        while (currentIndex != vertexOfInterest) {
            pathStack.push(currentIndex);
            currentIndex=edgeTo[currentIndex];
        }

        pathStack.push(vertexOfInterest);
        return pathStack;
    }
}
