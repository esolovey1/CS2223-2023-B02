package edu.wpi.cs2223.algorithms.week2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * An implementation of the Triple Sum (or Three Sum) problem.  The input to the problem is a linked
 * list of integers and the goal of the implementation is to find all distinct triples of integers that
 * sum to 0.
 *
 * This implementation should be the most straightforward way to solve the problem.  Performance
 * improvements will be considered elsewhere.
 *
 * To simplify things, it is safe to assume that all input values are unique.
 */
public class TripleSum {
    AtomicInteger nextCounter;

    LinkedNode<Integer> accessTrackingHead;

    public TripleSum(LinkedNode<Integer> head) {
        InstrumentorResult<Integer> instrumentorResult = new LinkedNodeInstrumentor<Integer>().instrument(head);
        this.nextCounter = instrumentorResult.nextCounter;
        this.accessTrackingHead = instrumentorResult.head;
    }

    /**
     * Returns a set of sets of integers.  Where each of the inner sets contain exactly three integers such
     * that the sum of these three integers is zero.  The external set should contain every possible combination
     * of 3 integers that sum to zero from the input linked list of integers.
     */
    public Set<Set<Integer>> find(){
        // TODO: implement
        Set<Set<Integer>> result = new HashSet<>();
        return result;
    }

    public int countOfNextInvocations(){
        return nextCounter.get();
    }

    public int runningTime(int elementCount) {
        // TODO: implement
        return 0;
    }
}
