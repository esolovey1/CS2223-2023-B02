package edu.wpi.cs2223.algorithms.week2;

/**
 * Array backed QuickFind implementation.
 */
public class QuickFindUF implements UnionFind{

    int[] id;
    int componentCount;

    @Override
    public boolean union(int p, int q) {
        int idP = id[p];
        int idQ = id[q];

        if (idP == idQ) {
            return true;
        }

        for (int a = 0; a < id.length; a++) {
            if (id[a] == idP){
                id[a] = idQ;
            }
        }

        componentCount--;
        return false;
    }

    @Override
    public int find(int p) {
        return id[p];
    }

    @Override
    public void initialize(int size) {
        id = new int[size];

        for (int a = 0; a < id.length; a++) {
            id[a] = a;
        }

        componentCount = size;
    }

    @Override
    public boolean connected(int p, int q) {
        return id[p] == id[q];
    }

    @Override
    public int componentCount() {
        return componentCount;
    }

    @Override
    public int countOfNextInvocations() {
        // not applicable to this algorithm
        return 0;
    }

    @Override
    public int runningTime(int nodeCount) {
        // TODO: implement
        return 0;
    }
}
