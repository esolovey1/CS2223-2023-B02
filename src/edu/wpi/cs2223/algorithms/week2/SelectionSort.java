package edu.wpi.cs2223.algorithms.week2;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Problem 1 : Selection Sort
 *
 * 1a) The run time of the selection sort implementation is quadratic because it iterates over the
 * unsorted portion of the list for each value in the list to find the next smallest value using comparisons.
 *
 * 1b) The run time of the algortithm does not vary based on what the input values are.
 * no matter the input values it has to check each element anyway to find the smallest unsorted element.
 *
 * 1c) When looking at the output of doublingPerformanceTest, the number of actual calls to 'next' directly
 * correlates to the expected running time, which looks like the elements squared. Selection Sort
 * calls to 'next' and closely follows the theoretical expected number of operations.
 *
 * 1d) in class we looked at the mathematical induction
 * For N=1, the running time T(1) is 0 - T(N) = N(N-1)/2.
 * T(N) holds true, so T(N) = N(N-1)/2.
 * T(N+1) = (N+1)N/2
 *         = (N(N-1)/2) + N (expanding T(N) and adding the next set of 'next' calls for the additional element)
 *         = T(N) + N
 * adding an additional element increases the running time by N 'next' calls, and
 * that the running time equation holds true for any N.
 */

public class SelectionSort<T extends Comparable<T>> {

    AtomicInteger nextCounter;

    LinkedNode<T> head;

    LinkedNode<T> accessTrackingHead;

    public SelectionSort(LinkedNode<T> head) {
        this.head = head;

        InstrumentorResult<T> instrumentorResult = new LinkedNodeInstrumentor<T>().instrument(head);
        this.nextCounter = instrumentorResult.nextCounter;
        this.accessTrackingHead = instrumentorResult.head;
    }


    public int runningTime(int elementCount) {
        return elementCount * (elementCount - 1) / 2;
    }

    /**
     *  AccessTrackingNode  should move nodes around
     * in this chain rather than creating new nodes or a new chain.
     */
    public LinkedNode<T> sort() {
        // we dont have anything to do if there is one thing to sort lol
        if (accessTrackingHead == null || accessTrackingHead.next() == null) {
            return accessTrackingHead;
        }


        T placeholderValue = null; // placeholder
        AccessTrackingNode<T> dummyHead = new AccessTrackingNode<>(nextCounter, placeholderValue, null);
        dummyHead.updateNext(accessTrackingHead); // make sure its the right type

        // last node
        LinkedNode<T> sortedTail = dummyHead;

        while (sortedTail.next() != null) {
            LinkedNode<T> smallest = sortedTail.next();
            LinkedNode<T> smallestPrevious = sortedTail;
            LinkedNode<T> current = smallest.next();
            LinkedNode<T> previous = smallest;

            // find the smallest node
            while (current != null) {
                if (current.value().compareTo(smallest.value()) < 0) {
                    smallest = current;
                    smallestPrevious = previous;
                }
                previous = current;
                current = current.next();
            }

            // If the smallest node is not the first node in the unsorted list, swap nodes
            if (smallest != sortedTail.next()) {
                // Remove smallest
                smallestPrevious.updateNext(smallest.next());
                // Insert smallest node at the beginning of the unsorted list
                smallest.updateNext(sortedTail.next());
                // Link the last sorted node to the new smallest node
                sortedTail.updateNext(smallest);
            }

            // Move the sortedTail forward
            sortedTail = sortedTail.next();
        }

        accessTrackingHead = dummyHead.next();
        return accessTrackingHead;
    }

    public int countOfNextInvocations(){
        return nextCounter.get();
    }
}
