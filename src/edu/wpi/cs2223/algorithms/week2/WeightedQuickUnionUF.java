package edu.wpi.cs2223.algorithms.week2;

import edu.wpi.cs2223.algorithms.week2.AccessTrackingNode;
import edu.wpi.cs2223.algorithms.week2.LinkedNode;
import edu.wpi.cs2223.algorithms.week2.UnionFind;

import java.util.concurrent.atomic.AtomicInteger;

public class WeightedQuickUnionUF implements UnionFind {
    LinkedNode<Integer>[] nodes;
    AtomicInteger nextCounter;

    int[] componentSize;

    int componentCount;

    @Override
    public void initialize(int size) {
        nextCounter = new AtomicInteger(0);

        nodes = ((LinkedNode<Integer>[]) new LinkedNode[size]);
        componentSize = new int[size];

        componentCount = size;

        for (int a = 0; a < size; a++){
            nodes[a] = new AccessTrackingNode<>(nextCounter, a, null);
            componentSize[a] = 1;
        }
    }

    LinkedNode<Integer> findRoot(int p) {
        LinkedNode<Integer> nodeP = nodes[p];

        LinkedNode<Integer> rootOfP = nodeP;
        while (rootOfP.next() != null) {
            rootOfP = rootOfP.next();
        }

        return rootOfP;
    }

    @Override
    public boolean union(int p, int q) {
        LinkedNode<Integer> rootOfP = findRoot(p);
        LinkedNode<Integer> rootOfQ = findRoot(q);

        if (rootOfP.value().equals(rootOfQ.value())) {
            return true;
        }

        // combine P and Q components
        if (componentSize[rootOfP.value()] > componentSize[rootOfQ.value()]) {
            // P is a larger component than Q, so we will re-root the Q component under P
            rootOfQ.updateNext(rootOfP);
            componentSize[rootOfP.value()] = componentSize[rootOfP.value()] + componentSize[rootOfQ.value()];
            componentSize[rootOfQ.value()] = 0;
        } else {
            // Q is a larger component than P, so we will re-root the P component under Q
            rootOfP.updateNext(rootOfQ);
            componentSize[rootOfQ.value()] = componentSize[rootOfP.value()] + componentSize[rootOfQ.value()];
            componentSize[rootOfP.value()] = 0;
        }

        componentCount--;
        return false;
    }

    @Override
    public boolean connected(int p, int q) {
        LinkedNode<Integer> rootOfP = findRoot(p);
        LinkedNode<Integer> rootOfQ = findRoot(q);

        return (rootOfP.value().equals(rootOfQ.value()));
    }

    @Override
    public int find(int p) {
        return findRoot(p).value();
    }

    @Override
    public int componentCount() {
        return componentCount;
    }

    public int countOfNextInvocations(){
        return nextCounter.get();
    }

    public int runningTime(int elementCount) {
        return (int)(elementCount * Math.log(elementCount));
    }
}
