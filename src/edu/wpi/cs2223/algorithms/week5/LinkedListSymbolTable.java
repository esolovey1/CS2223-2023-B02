package edu.wpi.cs2223.algorithms.week5;

import edu.wpi.cs2223.algorithms.week4.SymbolTable;

/**
 * Implements the SymbolTable interface via a backing Linked List.
 */
public class LinkedListSymbolTable<Key extends Comparable, Value> implements SymbolTable<Key, Value> {

    SymbolTableNode<Key, Value> head;

    @Override
    public void put(Key key, Value value) {
        // TODO: implement
    }

    @Override
    public Value get(Key key) {
        // TODO: implement
        return null;
    }

    @Override
    public Value delete(Key key) {
        // TODO: implement
        return null;
    }

    @Override
    public boolean contains(Key key) {
        // TODO: implement
        return false;
    }

    @Override
    public boolean isEmpty() {
        // TODO: implement
        return false;
    }

    @Override
    public int size() {
        // TODO: implement
        return 0;
    }

}
