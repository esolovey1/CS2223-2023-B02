package edu.wpi.cs2223.algorithms.week5;

import edu.wpi.cs2223.algorithms.week4.BSTNode;
import edu.wpi.cs2223.algorithms.week4.SymbolTable;

/**
 * Binary Search backed implementation of a Symbol Table.  Deletes are implemented lazily - nodes are deactivated
 * but remain in the tree.
 *
 * @param <Key> type of keys in the symbol table
 * @param <Value> type of values in the symbol table
 */
public class BSTSymbolTableLazyDelete<Key extends Comparable, Value> implements SymbolTable<Key, Value> {

    BSTNode<Key, Value> head;

    @Override
    public void put(Key key, Value value) {
        // TODO: implement
    }

    @Override
    public Value get(Key key) {
        // TODO: implement
        return null;
    }

    @Override
    public Value delete(Key key) {
        // TODO: implement
        return null;
    }

    @Override
    public boolean contains(Key key) {
        // TODO: implement
        return false;
    }

    @Override
    public boolean isEmpty() {
        // TODO: implement
        return false;
    }

    @Override
    public int size() {
        // TODO: implement
        return 0;
    }

    /**
     * Helper method to locate a node and its parent for a given key.  The parent reference is helpful when performing
     * manipulation on the BST.  If the key is found in the root, the parent reference will be null.
     */
    ParentAndNode locate(Key target){
        if (head == null){
            return new ParentAndNode(null, null);
        }

        BSTNode<Key, Value> current = head;
        BSTNode<Key, Value> previous = null;

        while (current != null){
            if (current.key.equals(target)) {
                return new ParentAndNode(previous, current);
            }

            previous = current;

            if (target.compareTo(current.key) < 0) {
                current = current.leftChild;
            } else {
                current = current.rightChild;
            }
        }

        return new ParentAndNode(previous, null);
    }

    /**
     * Simple, internal tuple of a node and its parent.
     */
    class ParentAndNode{
        final BSTNode<Key, Value> parent;
        final BSTNode<Key, Value> node;

        public ParentAndNode(BSTNode<Key, Value> parent, BSTNode<Key, Value> node) {
            this.parent = parent;
            this.node = node;
        }
    }

    /**
     * Builds up a string representation of this BST.  An attempt is made to make all spacing as true to the structure
     * of the tree as possible.  For example, the following invocations:
     * bstSymbolTable.put(6, "six");
     * bstSymbolTable.put(3, "three");
     * bstSymbolTable.put(8, "eight");
     *
     * Should result in a string that looks like this:
     *         6
     *       3   8
     */
    public String treeDiagram(){
        String[] keys = new String[10];
        buildDebugInfoForLevel(head, keys, 0);

        String result = "";
        int level = 0;

        int totalLevels = 0;
        while (keys[totalLevels] != null) {
            totalLevels++;
        }

        while (keys[level] != null) {
            String indent = "";
            for (int indentIndex = 0; indentIndex < (totalLevels - level); indentIndex++){
                indent += " ";
            }

            result = result + indent + keys[level] + " \n";
            level++;
        }

        return result;
    }

    /**
     * Helper for the treeDiagram utility.
     */
    void buildDebugInfoForLevel(BSTNode<Key, Value> node, String[] keys, int level) {
        if (node == null) {
            return;
        }

        if (keys[level] == null) {
            keys[level] = "";
        }

        keys[level] += " " + node.key;
        level++;

        buildDebugInfoForLevel(node.leftChild, keys, level);
        buildDebugInfoForLevel(node.rightChild, keys, level);
    }
}
